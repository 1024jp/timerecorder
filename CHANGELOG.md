CHANGELOG
======================

v3.1.0 (unreleased)
----------------------

- Support macOS 15 Sequoia.
- Change the system requirement to __macOS 14 and later__.


v3.0.0 (103) 2023-11-11
----------------------

### New Features

- Handle more precise time.
- Add a feature to display the list of stamped times.
- Add a feature to edit time.
- Add a feature to change the watch speed.
- Enable removing the selected rows in the time stamp table.
- Enable copying selected rows in the table.
- Enable to undo/redo the stamp list modification.
- Add English (United Kingdom) localization.


### Improvements

- Change the system requirement to __macOS 13 and later__.
- Enable selecting multiple table rows.
- Auto-scroll the table when stamping.
- Restore the elapsed time from the last session.
- Resize the default window size.
- Align table values to the right.


v2.1.0 (99) 2023-10-14
----------------------

- Support macOS 14 Sonoma.
- Make table rows selectable.


v2.0.2 (96) 2023-04-16
----------------------

- Support macOS 13 Ventura.


v2.0.1 (94) 2022-04-29
----------------------

- Fix English localization.


v2.0.0 (92) 2021-12-23
----------------------

- Change the system requirement to __macOS 12 Monterey and later__.
- Support Apple Silicon.
- Update the application icon.
- Migrate to SwiftUI.
- Add Japanese and German localizations.
- Fix preferences window layout.
- Fix some error messages.


v1.5.0 (88) 2018-09-22
----------------------

- Change the system requirement to __macOS 10.12.2 Sierra and later__.
- Support Dark Mode in macOS 10.14.


v1.4.3 (85) 2016-10-05
----------------------

- Fix application code sign.


v1.4.2 (78)
----------------------

- Enable Auto Termination.
- Disable AppNap.


v1.4.1 (70) 2015-04-09
----------------------

- Tweak user interface.


v1.4 (64) 2014-11-01
----------------------

- アプリケーションアイコンおよび書類アイコンをYosemiteスタイルに変更
- 環境設定のスタイルを変更
- 内部コードを Objective-C から Swift に変更
- 対応する OS のバージョンを OS X Mavericks 以降に変更
- 英語のヘルプコンテンツを追加


v1.3.2 (29) 2014-02-22
----------------------

- QuickTime Playerが立ち上がっていないときは、Start/StopメッセージをQuickTime Playerに送らないよう変更


v1.3.1 (26) 2014-01-15
----------------------

- ヘルプコンテンツを追加
- メニューにWebサイトへのリンクを追加


v1.3 (23) 2013-12-23
----------------------

- カウントのStart/StopでQuickTime Playerの最前面の動画もスタート/停止する機能を追加 (デフォルトではOFF）
- 環境設定パネルを追加
- テーブルビューが時間以外でソートされているときも、出力するCSVファイルは常に時間順になるように変更
- アプリケーションアイコンをブラッシュアップ
- 他、コードの改善


v1.2 (14) 2013-12-18
----------------------

- Timestamps(.timestamps)形式をネイティブフォーマットとし、CSV/TSVに書き出す場合は Export...経由で行なうように仕様を変更
- Timestampsファイルのオープン＆再開をサポート
    - 最終スタンプ時刻からの再開となります。(例: 7.55秒に最後のスタンプを押して 14.22秒にStop＆保存した場合、7秒から再開されます)
- セーブ時点からスタンプが追加されたときに Edited フラグが立つようにした
    - これにより、保存されていないウインドウを消そうとしたときにダイアログがでるようになったりする
- セーブパネルが出現した時自動的にレコーディングが止まるようにした
- RestartボタンをResumeボタンに変更（機能は変わらない。ラベルのみ）
- ウインドウサイズの調整


v1.1 (5) 2013-12-18
----------------------

- スタンプがない秒も自動的にレコードを生成するように変更
- Restart機能を追加
    - 例: 23.53秒に停止した場合、23.00秒からの再開（小数点以下切り捨て）。
      ただし、前回の23.00-23.53秒間のカウントは残りそこからカウントアップするので重複に注意。
    - ↑暫定的に上記の仕様にしたので、変更を希望する場合はその旨連絡ください。
- 保存形式に Raw Data (スタンプ時刻の改行区切りデータ) を追加
    - 将来既存のデータを読み込んで記録を再開する機能をつけた際にcsvデータと区別しやすいよう、拡張子は dat とした。
- Stopのショートカットキーにescキーを設定
- レイアウトの調整
- 他、コードの改善
