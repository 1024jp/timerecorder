//
//  SettingsView.swift
//  TimeRecorder
//
//  Created by 1024jp on 2021-12-01.
//
//  © 2021-2024 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI

enum DefaultsKey {
    
    static let synchronizesMoviePlayer = "enableMoviePlayerSync"
    static let speedRate = "speedRate"
}


struct SettingsView: View {
    
    @AppStorage(DefaultsKey.synchronizesMoviePlayer) private var synchronizesMoviePlayer = false
    @AppStorage(DefaultsKey.speedRate) private var speedRate = 1.0
    
    
    var body: some View {
        
        Form {
            Picker("Playback speed:", selection: $speedRate) {
                ForEach(PlaybackSpeed.allCases, content: \.labelView)
            }.fixedSize()
            
            LabeledContent("Video player:") {
                Toggle("Send play/pause messages to QuickTime Player", isOn: $synchronizesMoviePlayer)
            }
            .padding(.bottom, 8)
            
            HStack {
                Spacer()
                HelpLink(anchor: "sync_with_QuickTime_Player")
            }
        }
        .fixedSize()
        .scenePadding()
    }
}



// MARK: - Preview

#Preview {
    SettingsView()
}
