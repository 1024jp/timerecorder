//
//  ContentView.swift
//  TimeRecorder
//
//  Created by 1024jp on 2021-12-19.
//
//  © 2021-2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI

struct ContentView: View {
    
    fileprivate enum TableMode: String, CaseIterable {
        
        case timeStamp
        case bin
    }
    
    
    @EnvironmentObject private var document: TimeStampDocument
    @Environment(\.undoManager) private var undoManager
    
    @SceneStorage("ContentView.elapsedTime") private var elapsedTime: Double = 0
    @AppStorage("lastTableMode") private var lastTableMode: TableMode = .timeStamp
    @AppStorage(DefaultsKey.speedRate) private var defaultRate = 1.0
    
    @State private var watch = StopWatch()
    
    @State private var tableMode: TableMode = .timeStamp
    @State private var isTimeEditorPresented = false
    
    private let buttonWidth: Double = 64
    
    
    var body: some View {
        
        VStack(spacing: 0) {
            // time display
            VStack(spacing: 6) {
                TimerView(watch: $watch)
                    .font(.system(size: 28, weight: .medium, design: .rounded))
                    .monospacedDigit()
                    .contextMenu {
                        Button("Edit Time…") {
                            self.editTimeAction?()
                        }.disabled(self.editTimeAction == nil)
                    }
                
                Text(verbatim: "\(self.watch.rate)×")
                    .monospacedDigit()
                    .padding(.horizontal, 4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke())
                    .foregroundStyle(.secondary)
                    .opacity(self.watch.rate == 1 ? 0.5 : 1)
                    .contentTransition(.numericText())
                    .animation(.default, value: self.watch.rate)
                    .controlSize(.small)
                    .help("Playback speed")
            }
            .scenePadding(.horizontal)
            .padding(.bottom)
            
            // table switcher
            Divider()
            HStack {
                ForEach(TableMode.allCases, id: \.self) { mode in
                    TabPickerButtonView(mode.titleKey, systemImage: mode.systemImage, isSelected: self.tableMode == mode) {
                        self.tableMode = mode
                    }
                }
            }
            .padding(.vertical, 4)
            .padding(.horizontal)
            .frame(maxWidth: .infinity)
            .background(.background)
            Divider()
            
            // table
            switch self.tableMode {
                case .timeStamp:
                    TimeStampTable(document: self.document)
                case .bin:
                    let upperBound = max(self.watch.time, self.document.stamps.map(\.second).max() ?? 0)
                    BinTable(bins: self.document.stamps.map(\.second).bins(range: 0...Int(upperBound)))
            }
            
            // control
            HStack {
                if self.watch.isRunning {
                    Button(role: .cancel) {
                        self.watch.stop()
                    } label: {
                        Text("Pause")
                            .frame(minWidth: self.buttonWidth)
                    }
                    .keyboardShortcut(.cancelAction)
                    
                } else {
                    Button {
                        self.watch.start()
                    } label: {
                        Text(self.watch.time == 0 ? "Start" : "Resume")
                            .frame(minWidth: self.buttonWidth)
                    }
                    .keyboardShortcut(.defaultAction)
                }
                
                Button {
                    self.document.addStamp(Stamp(second: self.watch.time), undoManager: self.undoManager)
                    self.undoManager?.setActionName(String(localized: "Stamp"))
                } label: {
                    Text("Stamp")
                        .frame(minWidth: self.buttonWidth)
                }
                .keyboardShortcut(.defaultAction)
                .disabled(!self.watch.isRunning)
            }
            .fixedSize()
            .padding(.top)
            .scenePadding(.horizontal)
        }
        .onAppear {
            self.tableMode = self.lastTableMode
            if self.elapsedTime > 0 {
                self.watch.suspendedTime = self.elapsedTime
            } else if let lastTime = self.document.stamps.last?.second {
                self.watch.suspendedTime = lastTime
            }
            self.watch.rate = self.defaultRate
        }
        .onDisappear {
            self.watch.stop()
            self.elapsedTime = self.watch.time
        }
        .onChange(of: self.watch.suspendedTime) { (_, newValue) in
            self.elapsedTime = newValue
        }
        .onChange(of: self.defaultRate) { (_, newValue) in
            self.watch.rate = newValue
        }
        .onChange(of: self.tableMode) { (_, newValue) in
            self.lastTableMode = newValue
        }
        .sheet(isPresented: $isTimeEditorPresented) {
            TimeEditView(self.watch.suspendedTime) { time in
                self.watch.suspendedTime = time
            }
        }
        .focusedSceneValue(\.watch, $watch)
        .focusedSceneValue(\.editTimeAction, self.editTimeAction)
        .scenePadding(.vertical)
    }
    
    
    private var editTimeAction: (() -> Void)? {
        
        self.watch.isRunning ? nil : { self.isTimeEditorPresented = true }
    }
}


private struct TimerView: View {
    
    @Binding var watch: StopWatch
    
    
    var body: some View {
        
        if self.watch.isRunning {
            TimelineView(.periodic(from: Date(timeIntervalSinceNow: self.watch.time), by: 0.1 / self.watch.rate)) { _ in
                Text(self.timeDisplay)
            }
        } else {
            Text(self.timeDisplay)
        }
    }
    
    
    private var timeDisplay: AttributedString {
        
        Measurement(value: self.watch.time, unit: UnitDuration.seconds)
            .formatted(.measurement(width: .abbreviated,
                                    numberFormatStyle: .number.precision(.fractionLength(1))).attributed)
            .replacingAttributes(AttributeContainer.measurement(.unit),
                                 with: AttributeContainer.font(.system(size: 22)))
    }
}


private extension ContentView.TableMode {
    
    var titleKey: LocalizedStringKey {
        
        switch self {
            case .timeStamp: "Time Stamp"
            case .bin: "Bin"
        }
    }
    
    
    var systemImage: String {
        
        switch self {
            case .timeStamp: "stopwatch"
            case .bin: "chart.bar.xaxis"
        }
    }
}


private struct TabPickerButtonView: View {
    
    let titleKey: LocalizedStringKey
    let systemImage: String
    let isSelected: Bool
    let action: () -> Void
    
    @State private var isHovered = false
    
    
    init(_ titleKey: LocalizedStringKey, systemImage: String, isSelected: Bool, action: @escaping () -> Void) {
        
        self.titleKey = titleKey
        self.systemImage = systemImage
        self.isSelected = isSelected
        self.action = action
    }
    
    
    var body: some View {
        
        Button(action: self.action) {
            HStack(spacing: 4) {
                Image(systemName: self.systemImage)
                Text(self.titleKey)
                    .brightness(-0.05)
                    .fixedSize()
            }
        }
        .buttonStyle(.borderless)
        .tint(self.isSelected ? .accentColor : nil)
        .padding(.vertical, 2)
        .padding(.horizontal, 6)
        .background(self.background, in: RoundedRectangle(cornerRadius: 6))
        .onHover { self.isHovered = $0 }
    }
    
    
    private var background: Color {
        
        if self.isSelected {
            .accentColor.opacity(0.15)
        } else if self.isHovered {
            .primary.opacity(0.05)
        } else {
            .clear
        }
    }
}


private struct TimeStampTable: View {
    
    @StateObject var document: TimeStampDocument
    
    @Environment(\.undoManager) private var undoManager
    
    @State private var selection: Set<Stamp.ID> = []
    
    
    var body: some View {
        
        ScrollViewReader { proxy in
            Table(self.document.stamps, selection: $selection) {
                TableColumn("Seconds") {
                    Text($0.second, format: .number.precision(.fractionLength(2)))
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
            }
            .copyable(
                self.document.stamps
                    .filter { self.selection.contains($0.id) }
                    .map { $0.second.formatted() }
            )
            .onDeleteCommand {
                self.document.deleteStamps(ids: self.selection, undoManager: self.undoManager)
                self.undoManager?.setActionName(String(localized: "Remove Stamps"))
            }
            .onChange(of: self.document.stamps.last) { (_, newValue) in
                guard let newValue else { return }  // important to unwrap
                proxy.scrollTo(newValue.id)
            }
            .monospacedDigit()
        }
    }
}


private struct BinTable: View {
    
    let bins: [Bin]
    
    @State private var selection: Set<Bin.ID> = []
    
    
    var body: some View {
        
        ScrollViewReader { proxy in
            Table(self.bins, selection: $selection) {
                TableColumn("Seconds") {
                    Text($0.value, format: .number)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
                TableColumn("Count") {
                    Text($0.count, format: .number)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
            }
            .copyable(
                self.bins
                    .filter { self.selection.contains($0.id) }
                    .map { [$0.value, $0.count].map(String.init).joined(separator: "\t") }
            )
            .onChange(of: self.bins.last) { (_, newValue) in
                proxy.scrollTo(newValue?.id)
            }
            .monospacedDigit()
        }
    }
}



// MARK: - Preview

#Preview {
    ContentView()
        .environmentObject(TimeStampDocument(seconds: [0.5, 1.1, 1.3]))
        .frame(width: 220)
}
