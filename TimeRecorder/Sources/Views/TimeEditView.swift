//
//  TimeEditView.swift
//  TimeRecorder
//
//  Created by 1024jp on 2023-11-08.
//
//  © 2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI

struct TimeEditView: View {
    
    @Environment(\.dismiss) private var dismiss
    
    @State private var value: Double
    private let completion: (Double) -> Void
    
    
    init(_ value: Double, completion: @escaping (Double) -> Void) {
        
        self._value = State(wrappedValue: value)
        self.completion = completion
    }
    
    
    var body: some View {
        
        VStack {
            Stepper("Time:", value: $value, in: 0...(.infinity), step: 0.1,
                    format: .number.precision(.fractionLength(1)))
            .monospacedDigit()
            .multilineTextAlignment(.trailing)
            .padding(.bottom, 8)
            
            HStack {
                Spacer()
                
                Button("Cancel", role: .cancel) {
                    self.dismiss()
                }.keyboardShortcut(.cancelAction)
                
                Button("OK") {
                    self.dismiss()
                    self.completion(self.value)
                }.keyboardShortcut(.defaultAction)
            }
        }.scenePadding()
    }
}



// MARK: - Preview

#Preview {
    TimeEditView(0.5) { _ in }
        .frame(width: 200)
}
