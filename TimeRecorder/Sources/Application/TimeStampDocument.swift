//
//  TimeStampDocument.swift
//  TimeRecorder
//
//  Created by 1024jp on 2013-12-16.
//
//  © 2013-2024 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI
import UniformTypeIdentifiers

extension UTType {
    
    static let timeStamp = UTType(exportedAs: "com.wolfrosch.timerecorder.timestamps")
    
    
    var separator: String {
        
        switch self {
            case .timeStamp: "\n"
            case .commaSeparatedText: ","
            case .tabSeparatedText: "\t"
            default: preconditionFailure()
        }
    }
}


struct Stamp: Identifiable, Equatable {
    
    let id = UUID()
    let second: Double
}


@Observable final class TimeStampDocument: ReferenceFileDocument {
    
    typealias Snapshot = [Stamp]
    
    
    private(set) var stamps: [Stamp]
    
    static let readableContentTypes: [UTType] = [.timeStamp]
    static let writableContentTypes: [UTType] = [.timeStamp, .commaSeparatedText, .tabSeparatedText]
    
    
    // MARK: Reference File Document Methods
    
    init(seconds: [Double] = []) {
        
        self.stamps = seconds.map(Stamp.init(second:))
    }
    
    
    init(configuration: ReadConfiguration) throws {
        
        guard
            let data = configuration.file.regularFileContents,
            let string = String(data: data, encoding: .utf8)
        else { throw CocoaError(.fileReadCorruptFile) }
        
        self.stamps = string.components(separatedBy: .newlines)
            .compactMap(Double.init)
            .map(Stamp.init(second:))
    }
    
    
    func fileWrapper(snapshot: [Stamp], configuration: WriteConfiguration) throws -> FileWrapper {
        
        let seconds = snapshot.map(\.second)
        let lines: [String] = switch configuration.contentType {
            case .timeStamp:
                seconds
                    .map { $0.formatted(.number.precision(.fractionLength(4))) }
            default:
                Dictionary(grouping: seconds, by: Int.init)
                    .sorted { $0.key < $1.key }  // sort in chronological order
                    .map { [String($0.key), String($0.value.count)] }
                    .map { $0.joined(separator: configuration.contentType.separator) }
        }
        
        let data = Data(lines.joined(separator: "\n").utf8)
        
        return .init(regularFileWithContents: data)
    }
    
    
    func snapshot(contentType: UTType) throws -> [Stamp] {
        
        self.stamps
    }
    
    
    // MARK: Public Methods
    
    func addStamp(_ stamp: Stamp, undoManager: UndoManager? = nil, animation: Animation? = .default) {
        
        withAnimation(animation) {
            self.stamps.append(stamp)
        }
        
        undoManager?.registerUndo(withTarget: self) { target in
            withAnimation(animation) {
                _ = target.stamps.removeLast()
            }
            undoManager?.registerUndo(withTarget: target) { target in
                target.addStamp(stamp, undoManager: undoManager, animation: animation)
            }
        }
    }
    
    
    func deleteStamps(ids: Set<Stamp.ID>, undoManager: UndoManager? = nil, animation: Animation? = .default) {
        
        let oldStamps = self.stamps
        
        withAnimation(animation) {
            self.stamps.removeAll { ids.contains($0.id) }
        }
        
        undoManager?.registerUndo(withTarget: self) { target in
            target.replaceStamps(with: oldStamps, undoManager: undoManager, animation: animation)
        }
    }
    
    
    // MARK: Private Methods
    
    private func replaceStamps(with stamps: [Stamp], undoManager: UndoManager? = nil, animation: Animation? = .default) {
        
        let oldStamps = self.stamps
        
        withAnimation(animation) {
            self.stamps = stamps
        }
        
        undoManager?.registerUndo(withTarget: self) { target in
            target.replaceStamps(with: oldStamps, undoManager: undoManager, animation: animation)
        }
    }
}
