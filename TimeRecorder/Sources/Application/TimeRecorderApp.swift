//
//  TimeRecorderApp.swift
//  TimeRecorder
//
//  Created by 1024jp on 2013-12-22.
//
//  © 2013-2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI

private enum Bookmark {
    
    static let webSite = URL(string: "https://bitbucket.org/1024jp/timerecorder")!
}


@main
struct TimeRecorderApp: App {
    
    var body: some Scene {
        
        Settings {
            SettingsView()
        }
        .commands {
            CommandGroup(after: .help) {
                Link("TimeRecorder Website", destination: Bookmark.webSite)
            }
        }
        
        DocumentGroup {
            TimeStampDocument()
        } editor: { _ in
            ContentView()
        }
        .defaultSize(width: 240, height: 400)
        .commands {
            WatchCommands()
        }
    }
}



// MARK: Commands

struct WatchCommands: Commands {
    
    @FocusedBinding(\.watch) private var watch
    @FocusedValue(\.editTimeAction) private var editTimeAction
    
    
    var body: some Commands {
        
        CommandGroup(after: .textEditing) {
            Picker("Playback Speed", selection: Binding(get: { self.watch?.rate ?? 0 }, set: { self.watch?.rate = $0 })) {
                ForEach(PlaybackSpeed.allCases, content: \.labelView)
            }.disabled(self.watch?.isRunning != false)
            
            Button("Edit Time…") {
                self.editTimeAction?()
            }.disabled(self.editTimeAction == nil)
        }
    }
}


extension FocusedValues {
    
    @Entry var watch: Binding<StopWatch>?
    @Entry var editTimeAction: (() -> Void)?
}
