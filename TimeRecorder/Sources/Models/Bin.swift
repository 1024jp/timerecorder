//
//  Bin.swift
//  TimeRecorder
//
//  Created by 1024jp on 2023-11-07.
//
//  © 2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

struct Bin: Equatable {
    
    var value: Int
    var count: Int
}


extension Bin: Identifiable {
    
    var id: Int { self.value }
}


extension RandomAccessCollection<Double> {
    
    func bins(range: ClosedRange<Int>) -> [Bin] {
        
        range.map { value in
            Bin(value: value, count: self.filter({ Int($0) == value }).count)
        }
    }
}
