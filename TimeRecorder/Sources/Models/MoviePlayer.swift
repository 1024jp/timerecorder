//
//  MoviePlayer.swift
//  TimeRecorder
//
//  Created by 1024jp on 2013-12-22.
//
//  © 2013-2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import AppKit

final class MoviePlayer: Sendable {
    
    enum Command: String {
        
        case play, pause
    }
    
    
    static let shared = MoviePlayer()
    
    private let playerIdentifier = "com.apple.QuickTimePlayerX"
    
    
    // MARK: Public Methods
    
    func send(command: Command) {
        
        guard
            UserDefaults.standard.bool(forKey: DefaultsKey.synchronizesMoviePlayer),
            !NSRunningApplication.runningApplications(withBundleIdentifier: self.playerIdentifier).isEmpty
        else { return }
        
        NSAppleScript(source: "tell application id \"\(self.playerIdentifier)\" to tell front document to \(command.rawValue)")?.executeAndReturnError(nil)
    }
}
