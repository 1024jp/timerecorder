//
//  StopWatch.swift
//  TimeRecorder
//
//  Created by 1024jp on 2023-11-09.
//
//  © 2023 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import Foundation

struct StopWatch {
    
    var suspendedTime: Double = 0
    var rate: Double = 1
    
    private var startedDate: Date?
    
    
    /// Whether the stop watch is running.
    var isRunning: Bool {
        
        self.startedDate != nil
    }
    
    
    /// The elapsed time in seconds.
    var time: Double {
        
        self.suspendedTime + self.currentLapse
    }
    
    
    /// Stop the watch.
    mutating func stop() {
        
        self.suspendedTime += self.currentLapse
        self.startedDate = nil
        
        MoviePlayer.shared.send(command: .pause)
    }
    
    
    /// Resume the watch.
    mutating func start() {
        
        self.startedDate = .now
        
        MoviePlayer.shared.send(command: .play)
    }
    
    
    /// The elapsed time since the last start by considering the speed rate.``
    private var currentLapse: Double {
        
        self.startedDate?.timeIntervalSinceNow.scaled(by: -self.rate) ?? 0
    }
}
