//
//  PlaybackSpeed.swift
//  TimeRecorder
//
//  Created by 1024jp on 2023-11-09.
//
//  © 2023-2024 1024jp
//  This program is licensed under the MIT license. See LICENSE file for details.
//

import SwiftUI

enum PlaybackSpeed: Double, CaseIterable {
    
    case half = 0.5
    case normal = 1.0
    case fast = 1.25
    case faster = 1.5
    case double = 2.0
}


// MARK: SwiftUI support

extension PlaybackSpeed: Identifiable {
    
    var id: Double { self.rawValue }
    
    
    var labelView: some View {
        
        (Text(self.label) + Text(verbatim: " (\(self.rawValue)×)").foregroundStyle(.secondary))
            .monospacedDigit()
    }
    
    
    private var label: String {
        
        switch self {
            case .half: String(localized: "PlaybackSpeed.Half", defaultValue: "Half")
            case .normal: String(localized: "PlaybackSpeed.Normal", defaultValue: "Normal")
            case .fast: String(localized: "PlaybackSpeed.Fast", defaultValue: "Fast")
            case .faster: String(localized: "PlaybackSpeed.Faster", defaultValue: "Faster")
            case .double: String(localized: "PlaybackSpeed.Double", defaultValue: "Double")
        }
    }
}



// MARK: - Preview

#Preview {
    VStack(alignment: .leading) {
        ForEach(PlaybackSpeed.allCases, content: \.labelView)
    }
    .padding()
}
