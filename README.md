# TimeRecorder

TimeRecorder is a tally counter app for macOS. It records every elapsed time from the start time when the button is clicked.

__requirement__: macOS 13 Ventura or higher
- __Mac App Store__: <https://apps.apple.com/jp/app/timerecorder/id936230545?l=en-US&mt=12>
- __Languages__: English, German, Japanese

<img src="screenshot@2x.png" width="322" alt="screenshot"/>


## License

© 2013-2023 1024jp.

The source code is licensed under the terms of the __MIT License__. See [LICENSE](LICENSE) for details.
